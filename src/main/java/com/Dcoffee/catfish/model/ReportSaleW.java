/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportSaleW {
    private String weekStart;
    private double totalSum;
    
public ReportSaleW(String weekStart,double totalSum) {
        this.weekStart = weekStart;
        this.totalSum = totalSum;
    }
    public ReportSaleW() {
        this("",0);
    }

    public String getWeekStart() {
        return weekStart;
    }

    public void setWeekStart(String weekStart) {
        this.weekStart = weekStart;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }


    @Override
    public String toString() {
        return "ReportSaleD{" + "weekStart=" + weekStart + ", totalSum=" + totalSum + '}';
    }
    public static ReportSaleW fromRS(ResultSet rs) {
        ReportSaleW obj = new ReportSaleW();
        try {
            
            obj.setWeekStart(rs.getString("WeekStart"));
            obj.setTotalSum(rs.getInt("total_sum"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportSaleW.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
}
