/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import com.Dcoffee.catfish.service.CustomerService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class Customer {

    private int cusId;
    private String cusName;
    private String cusSurname;
    private String cusTel;
    private int cusPoint;

    public Customer(int cusId, String cusName, String cusSurname, String cusTel, int cusPoint) {
        this.cusId = cusId;
        this.cusName = cusName;
        this.cusSurname = cusSurname;
        this.cusTel = cusTel;
        this.cusPoint = cusPoint;

    }

    public Customer(String cusName, String cusSurname, String cusTel, int cusPoint) {
        this.cusId = -1;
        this.cusName = cusName;
        this.cusSurname = cusSurname;
        this.cusTel = cusTel;
        this.cusPoint = cusPoint;
     
    }

    public Customer() {
        this.cusId = -1;
        this.cusName = "";
        this.cusSurname = "";
        this.cusTel = "";
        this.cusPoint = 0;
     
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusSurname() {
        return cusSurname;
    }

    public void setCusSurname(String cusSurname) {
        this.cusSurname = cusSurname;
    }

    public String getCusTel() {
        return cusTel;
    }

    public void setCusTel(String cusTel) {
        this.cusTel = cusTel;
    }

    public int getCusPoint() {
        return cusPoint;
    }

    public void setCusPoint(int cusPoint) {
        this.cusPoint = cusPoint;
    }

   

    @Override
    public String toString() {
        return "Customer ID  : "+cusId+"\n"
                + "Name : "+cusName+"\n"
                + "Surname : "+cusSurname+"\n"
                + "Telephone No. : "+cusTel+"\n"
                + "Point : "+cusPoint;
               
               
    }

    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setCusId(rs.getInt("cus_id"));
            customer.setCusName(rs.getString("cus_name"));
            customer.setCusSurname(rs.getString("cus_surname"));
            customer.setCusTel(rs.getString("cus_tel"));
            customer.setCusPoint(rs.getInt("cus_point"));
        
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }

    public static List<Customer> getCustomerList() {
        CustomerService customerService = new CustomerService();
        List<Customer> list = customerService.getCustomers();
        return list;
    }
}

