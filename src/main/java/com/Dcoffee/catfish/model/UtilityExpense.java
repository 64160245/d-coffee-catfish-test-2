/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class UtilityExpense {

    private int utilityExpenseId;
    private int utilityExpenseType;
    private String utilityExpensePeriod;
    private double utilityExpenseUnit;
    private double utilityExpenseTotal;

    public UtilityExpense(int utilityExpenseId, int utilityExpenseType, String utilityExpensePeriod, double utilityExpenseUnit, double utilityExpenseTotal) {
        this.utilityExpenseId = utilityExpenseId;
        this.utilityExpenseType = utilityExpenseType;
        this.utilityExpensePeriod = utilityExpensePeriod;
        this.utilityExpenseUnit = utilityExpenseUnit;
        this.utilityExpenseTotal = utilityExpenseTotal;
    }

    public UtilityExpense(int utilityExpenseType, String utilityExpensePeriod, double utilityExpenseUnit, double utilityExpenseTotal) {
        this.utilityExpenseId = -1;
        this.utilityExpenseType = utilityExpenseType;
        this.utilityExpensePeriod = utilityExpensePeriod;
        this.utilityExpenseUnit = utilityExpenseUnit;
        this.utilityExpenseTotal = utilityExpenseTotal;
    }

    public UtilityExpense() {
        this.utilityExpenseId = -1;
        this.utilityExpenseType = 0;
        this.utilityExpensePeriod = "";
        this.utilityExpenseUnit = 0;
        this.utilityExpenseTotal = 0;
    }

    public int getUtilityExpenseId() {
        return utilityExpenseId;
    }

    public void setUtilityExpenseId(int utilityExpenseId) {
        this.utilityExpenseId = utilityExpenseId;
    }

    public int getUtilityExpenseType() {
        return utilityExpenseType;
    }

    public void setUtilityExpenseType(int utilityExpenseType) {
        this.utilityExpenseType = utilityExpenseType;
    }

    public String getUtilityExpensePeriod() {
        return utilityExpensePeriod;
    }

    public void setUtilityExpensePeriod(String utilityExpensePeriod) {
        this.utilityExpensePeriod = utilityExpensePeriod;
    }

    public double getUtilityExpenseUnit() {
        return utilityExpenseUnit;
    }

    public void setUtilityExpenseUnit(double utilityExpenseUnit) {
        this.utilityExpenseUnit = utilityExpenseUnit;
    }

    public double getUtilityExpenseTotal() {
        return utilityExpenseTotal;
    }

    public void setUtilityExpenseTotal(double utilityExpenseTotal) {
        this.utilityExpenseTotal = utilityExpenseTotal;
    }

    @Override
    public String toString() {
        return "UtilityExpense{" + "utilityExpenseId=" + utilityExpenseId + ", utilityExpenseType=" + utilityExpenseType + ", utilityExpensePeriod=" + utilityExpensePeriod + ", utilityExpenseUnit=" + utilityExpenseUnit + ", utilityExpenseTotal=" + utilityExpenseTotal + '}';
    }

  

    

    public static UtilityExpense fromRS(ResultSet rs) {
        UtilityExpense utilityexpense = new UtilityExpense();
        try {
            utilityexpense.setUtilityExpenseId(rs.getInt("utility_expense_id"));
            utilityexpense.setUtilityExpenseType(rs.getInt("utility_expense_type"));
            utilityexpense.setUtilityExpensePeriod(rs.getString("utility_expense_period"));
            utilityexpense.setUtilityExpenseUnit(rs.getInt("utility_expense_unit"));
            utilityexpense.setUtilityExpenseTotal(rs.getDouble("utility_expense_total"));
        } catch (SQLException ex) {
            Logger.getLogger(UtilityExpense.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return utilityexpense;
    }

}
