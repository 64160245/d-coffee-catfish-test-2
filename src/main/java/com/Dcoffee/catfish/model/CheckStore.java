/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import com.Dcoffee.catfish.dao.EmployeeDao;
import com.Dcoffee.catfish.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class CheckStore {

    private int id;
//    private Date checkstoreDate;
    private String checkstoreDate;
    private int userId;
    private User user;
    private ArrayList<CheckStoreDetail> checkStoreDetail = new ArrayList<CheckStoreDetail>();

    public CheckStore(int id, String checkstoreDate, int userId) {
        this.id = id;
        this.checkstoreDate = checkstoreDate;
        this.userId = userId;
    }

    
    public CheckStore(String checkstoreDate, int userId) {
        this.id = -1;
        this.checkstoreDate = checkstoreDate;
        this.userId = userId;
    }
    

    public CheckStore() {
        this.id = -1;
        this.checkstoreDate = null;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCheckstoreDate() {
        return checkstoreDate;
    }

    public void setCheckstoreDate(String checkstoreDate) {
        this.checkstoreDate = checkstoreDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<CheckStoreDetail> getCheckStoreDetail() {
        return checkStoreDetail;
    }

    public void setCheckStoreDetail(ArrayList<CheckStoreDetail> checkStoreDetail) {
        this.checkStoreDetail = checkStoreDetail;
    }
    
    public void addCheckStoreDetail(Material material, int qty) {
       CheckStoreDetail cd = new CheckStoreDetail(1, material.getMaterialID(), material.getMaterialName(), 1);
        checkStoreDetail.add(cd);

    }
    
    public void addCheckStoreDetail(CheckStoreDetail checkStoreDetails) {
        checkStoreDetail.add(checkStoreDetails);
    }

    @Override
    public String toString() {
        return "CheckStore{" + "id=" + id + ", checkstoreDate=" + checkstoreDate + ", userId=" + userId + ", user=" + user + ", checkStoreDetail=" + checkStoreDetail + '}';
    }

   

     public static CheckStore fromRS(ResultSet rs) {
        CheckStore checkStore = new CheckStore();
        
        try {
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//            String in = rs.getString("check_store_date");
//             try {
//                checkStore.setCheckstoreDate(df.parse(in));
//            } catch (ParseException ex) {
//                Logger.getLogger(CheckStore.class.getName()).log(Level.SEVERE, null, ex);
//            }
            checkStore.setCheckstoreDate(rs.getString("check_store_date"));
            checkStore.setId(rs.getInt("check_store_id"));
            checkStore.setUserId(rs.getInt("user_id"));
//             Population
            UserDao userDao = new UserDao();
            User user = userDao.get(checkStore.getUserId());
            checkStore.setUser(user);
            
     } catch (SQLException ex) {
            Logger.getLogger(CheckStore.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStore;
    }
}
