/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import com.Dcoffee.catfish.dao.ProductDao;
import com.Dcoffee.catfish.service.ProductService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Product {
    private int id;
    private String productName;
    private double productPrice;
    private String productSize;
    private String productSweetLevel;
    private String productType;
    private int categoryId;

    public Product(int id, String productName, double productPrice, String productSize, String productSweetLevel, String productType, int categoryId) {
        this.id = id;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.productSweetLevel = productSweetLevel;
        this.productType = productType;
        this.categoryId = categoryId;
    }

     public Product(String productName, double productPrice, String productSize, String productSweetLevel, String productType, int categoryId) {
        this.id = -1;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.productSweetLevel = productSweetLevel;
        this.productType = productType;
        this.categoryId = categoryId;
    }

    public Product() {
        this.id = -1;
        this.productName = "";
        this.productPrice = 0;
        this.productSize = "";
        this.productSweetLevel = "";
        this.productType = "";
        this.categoryId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductSweetLevel() {
        return productSweetLevel;
    }

    public void setProductSweetLevel(String productSweetLevel) {
        this.productSweetLevel = productSweetLevel;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", productName=" + productName + ", productPrice=" + productPrice + ", productSize=" + productSize + ", productSweetLevel=" + productSweetLevel + ", productType=" + productType + ", categoryId=" + categoryId + '}';
    }
    
    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setProductName(rs.getString("product_name"));
            product.setProductPrice(rs.getDouble("product_price"));
            product.setProductSize(rs.getString("product_size"));
            product.setProductSweetLevel(rs.getString("product_sweet_level"));
            product.setProductType(rs.getString("product_type"));
            product.setCategoryId(rs.getInt("category_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
    public static List<Product> getProductList() {
        ProductService prdService = new ProductService();
        List<Product> list = prdService.getProducts();
        return list;
    }
    public static ArrayList<Product> getMockProductList() {
        ProductDao productDao = new ProductDao();
        ArrayList<Product> list = new ArrayList<>();
        for (Product product : productDao.getAll()) {
            list.add(product);
        }
        return list;
    }
    public boolean isValid(){
        return this.productName.length()>0 &&
                this.productSize.length()>0 &&
                this.productSweetLevel.length()>0 &&
                this.productType.length()>0 &&
                this.productPrice > 0.0;
    }
}




