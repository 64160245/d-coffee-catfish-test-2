/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.Data;

/**
 *
 * @author Miso
 */
public class ReportTotalCustomer {
    private int id;
    private String cusName;
    private String surName;
    private double totalPurchases;
    private int tatalOrder;

    public ReportTotalCustomer(int id, String cusName, String surName, double totalPurchases,int tatalOrder) {
        this.id = id;
        this.cusName = cusName;
        this.surName = surName;
        this.totalPurchases = totalPurchases;
        this.tatalOrder =tatalOrder;
    }

    public ReportTotalCustomer(String cusName, String surName, double totalPurchases,int tatalOrder) {
        this.cusName = cusName;
        this.surName = surName;
        this.totalPurchases = totalPurchases;
        this.tatalOrder =tatalOrder;
    }
      public ReportTotalCustomer() {
        this(-1,"","",0,0);
    }    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public double getTotalPurchases() {
        return totalPurchases;
    }

    public void setTotalPurchases(double totalPurchases) {
        this.totalPurchases = totalPurchases;
    }

    public int getTatalOrder() {
        return tatalOrder;
    }

    public void setTatalOrder(int tatalOrder) {
        this.tatalOrder = tatalOrder;
    }

    @Override
    public String toString() {
        return "ReportTotalCustomer{" + "id=" + id + ", cusName=" + cusName + ", surName=" + surName + ", totalPurchases=" + totalPurchases + ", tatalOrder=" + tatalOrder + '}';
    }

   
     public static ReportTotalCustomer fromRS(ResultSet rs) {
        ReportTotalCustomer reportTotalCustomer = new ReportTotalCustomer();
        try {
            reportTotalCustomer.setId(rs.getInt("cus_id"));
            reportTotalCustomer.setCusName(rs.getString("cus_name"));
            reportTotalCustomer.setSurName(rs.getString("cus_surname"));
            reportTotalCustomer.setTotalPurchases(rs.getDouble("total_purchases"));
            reportTotalCustomer.setTatalOrder(rs.getInt("total_orders"));
        } catch (SQLException ex) {
            Logger.getLogger(ReportTotalCustomer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reportTotalCustomer;
    } 
    
    
}
