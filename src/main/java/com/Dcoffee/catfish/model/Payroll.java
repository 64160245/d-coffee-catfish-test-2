/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.Data;

/**
 *
 * @author User
 */
public class Payroll {

    private int id;
    private Date date;
    private double amount;
    private int employeeId;

    public Payroll(int id, Date date, double amount, int employeeId) {
        this.id = id;
        this.date = date;
        this.amount = amount;
        this.employeeId = employeeId;
    }

    public Payroll(Date date, double amount, int employeeId) {
        this.id = -1;
        this.date = date;
        this.amount = amount;
        this.employeeId = employeeId;
    }

    public Payroll() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "Payroll{" + "id=" + id + ", date=" + date + ", amount=" + amount + ", employeeId=" + employeeId + '}';
    }



    public static Payroll fromRS(ResultSet rs) {
        Payroll payroll = new Payroll();
        try {
            payroll.setId(rs.getInt("payroll_id"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = rs.getString("payroll_date");
            try {
                payroll.setDate(df.parse(date));
            } catch (ParseException ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            }
            payroll.setAmount(rs.getDouble("payroll_amount"));
            payroll.setEmployeeId(rs.getInt("employee_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return payroll;
    }
}
