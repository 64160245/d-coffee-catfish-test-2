/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportSaleD {
    private String day;
    private double totalSum;
    
public ReportSaleD(String day,double totalSum) {
        this.day = day;
        this.totalSum = totalSum;
    }
    public ReportSaleD() {
        this("",0);
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    @Override
    public String toString() {
        return "ReportSaleD{" + "day=" + day + ", totalSum=" + totalSum + '}';
    }
    public static ReportSaleD fromRS(ResultSet rs) {
        ReportSaleD obj = new ReportSaleD();
        try {
            
            obj.setDay(rs.getString("Day"));
            obj.setTotalSum(rs.getInt("total_sum"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportSaleD.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
}
