/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fewxi
 */
public class Material {

    private int materialID;
    private String materialName;
    private int materialMin;
    private double materialTotalPrice;
    private String materialUnit;
    private double materialPrice;
    private int materialQty;

    public Material(int materialID, String materialName, int materialMin, double materialTotalPrice, String materialUnit, double materialPrice, int materialQty) {
        this.materialID = materialID;
        this.materialName = materialName;
        this.materialMin = materialMin;
        this.materialTotalPrice = materialTotalPrice;
        this.materialUnit = materialUnit;
        this.materialPrice = materialPrice;
        this.materialQty = materialQty;
    }

    public Material() {
        this.materialID = -1;
        this.materialName = "";
        this.materialMin = 0;
        this.materialTotalPrice = 0;
        this.materialUnit = "";
        this.materialPrice = 0;
        this.materialQty = 0;
    }

    public Material(String materialName, int materialMin, double materialTotalPrice, String materialUnit, double materialPrice, int materialQty) {
        this.materialID = -1;
        this.materialName = materialName;
        this.materialMin = materialMin;
        this.materialTotalPrice = materialTotalPrice;
        this.materialUnit = materialUnit;
        this.materialPrice = materialPrice;
        this.materialQty = materialQty;
    }

    public int getMaterialID() {
        return materialID;
    }

    public void setMaterialID(int materialID) {
        this.materialID = materialID;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public int getMaterialMin() {
        return materialMin;
    }

    public void setMaterialMin(int materialMin) {
        this.materialMin = materialMin;
    }

    public double getMaterialTotalPrice() {
        return materialTotalPrice;
    }

    public void setMaterialTotalPrice(double materialTotalPrice) {
        this.materialTotalPrice = materialTotalPrice;
    }

    public String getMaterialUnit() {
        return materialUnit;
    }

    public void setMaterialUnit(String materialUnit) {
        this.materialUnit = materialUnit;
    }

    public double getMaterialPrice() {
        return materialPrice;
    }

    public void setMaterialPrice(double materialPrice) {
        this.materialPrice = materialPrice;
    }

    public int getMaterialQty() {
        return materialQty;
    }

    public void setMaterialQty(int materialQty) {
        this.materialQty = materialQty;
    }

    @Override
    public String toString() {
        return "Material{" + "materialID=" + materialID + ", materialName=" + materialName + ", materialMin=" + materialMin + ", materialTotalPrice=" + materialTotalPrice + ", materialUnit=" + materialUnit + ", materialPrice=" + materialPrice + ", materialQty=" + materialQty + '}';
    }

    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setMaterialID(rs.getInt("material_id"));
            material.setMaterialName(rs.getString("material_name"));
            material.setMaterialMin(rs.getInt("material_min"));
            material.setMaterialTotalPrice(rs.getDouble("material_totalPrice"));
            material.setMaterialUnit(rs.getString("material_unit"));
            material.setMaterialPrice(rs.getDouble("material_price"));
            material.setMaterialQty(rs.getInt("material_qty"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }
}
