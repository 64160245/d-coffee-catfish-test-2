/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportProduct {
    private String Name;
    private int TotalAmount;

    public ReportProduct(String Name, int TotalAmount) {
        this.Name = Name;
        this.TotalAmount = TotalAmount;
    }
    public ReportProduct() {
        this("",0);
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(int TotalAmount) {
        this.TotalAmount = TotalAmount;
    }

    @Override
    public String toString() {
        return "ReportProduct{" + "Name=" + Name + ", TotalAmount=" + TotalAmount + '}';
    }
    public static ReportProduct fromRS(ResultSet rs) {
        ReportProduct obj = new ReportProduct();
        try {
            
            obj.setName(rs.getString("product_name"));
            obj.setTotalAmount(rs.getInt("total_amount"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportProduct.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
    
}
