/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fewxi
 */
public class Promotion {

    private int promoId;
    private String promoName;
    private double promoDiscount;

    public Promotion(int promoId, String promoName, double promoDiscount) {
        this.promoId = promoId;
        this.promoName = promoName;
        this.promoDiscount = promoDiscount;
    }
    public Promotion(String promoName, double promoDiscount) {
        this.promoId = -1;
        this.promoName = promoName;
        this.promoDiscount = promoDiscount;
    }
    public Promotion() {
        this.promoId = -1;
        this.promoName = "";
        this.promoDiscount = 0.0;
    }

    public int getPromoId() {
        return promoId;
    }

    public void setPromoId(int promoId) {
        this.promoId = promoId;
    }

    public String getPromoName() {
        return promoName;
    }

    public void setPromoName(String promoName) {
        this.promoName = promoName;
    }

    public double getPromoDiscount() {
        return promoDiscount;
    }

    public void setPromoDiscount(double promoDiscount) {
        this.promoDiscount = promoDiscount;
    }

    @Override
    public String toString() {
        return "Promotion{" + "promoId=" + promoId + ", promoName=" + promoName + ", promoDiscount=" + promoDiscount + '}';
    }
    
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
           promotion.setPromoId(rs.getInt("promo_id"));
           promotion.setPromoName(rs.getString("promo_name"));
           promotion.setPromoDiscount(rs.getDouble("promo_discount"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }

}
