/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.test;

import com.Dcoffee.catfish.dao.PayrollDao;
import com.Dcoffee.catfish.model.Payroll;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Miso
 */
public class TestPayrollDao {
        public static void main(String[] args) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateFormat = "2023-10-14 18:00";
        Date date = df.parse(dateFormat);
        Payroll payroll = new Payroll(date, 9000,1);
        PayrollDao payrollDao = new PayrollDao();

        // Test save
        payrollDao.save(payroll);

        // Test get
//        Payroll updatePayroll = payrollDao.get(id);
//        System.out.println(updatePayroll);

        // Test update
//        updatePayroll.setAmount(300);
//        payrollDao.update(updatePayroll);

        // Test delete
//        payrollDao.delete(payrollDao.get(id));
    }
    
}
