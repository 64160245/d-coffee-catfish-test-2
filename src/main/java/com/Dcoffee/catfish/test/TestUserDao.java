/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.test;

import com.Dcoffee.catfish.dao.UserDao;
import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.User;

/**
 *
 * @author werapan
 */
public class TestUserDao {

    public static void main(String[] args) {
        //getall
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        System.out.println("________________________________");
        //getone
        User user1 = userDao.get(13);
        System.out.println(user1);

//        User newUser = new User("user3", "password", 2, "F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("M");
        user1.setGender("F");
        userDao.update(user1);
        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
//        userDao.delete(user1);
//        for(User u: userDao.getAll()) {
//            System.out.println(u);
//        }
        for (User u : userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc ")) {
            System.out.println(u);
        }

        DatabaseHelper.close();
    }
}
