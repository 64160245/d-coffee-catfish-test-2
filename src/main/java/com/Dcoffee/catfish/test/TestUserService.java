/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.test;

import com.Dcoffee.catfish.model.User;
import com.Dcoffee.catfish.service.UserService;

/**
 *
 * @author werapan
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userSevice = new UserService();
        User user = userSevice.login("Pimlada", "password");
        if(user!=null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.out.println("Error");
        }
        
    }
}
