/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.test;

import com.Dcoffee.catfish.dao.CheckTimeDao;
import com.Dcoffee.catfish.model.CheckTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Miso
 */
public class TestCheckTimeDao {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateFormatIn = "2023-11-9 08:00";
        String dateFormatOut = "2023-11-9  16:00";
        Date in = df.parse(dateFormatIn);
        Date out = df.parse(dateFormatOut);
        CheckTime checktime = new CheckTime(in,out,9,"n",1,2);
        CheckTimeDao checktimeDao = new CheckTimeDao();
        
        // Test save
//        checktimeDao.save(checktime);
        
//        // Test get
//        CheckTime updateCheckTime = checktimeDao.get(1);
//        System.out.println(updateCheckTime);
//        
//        // Test update
//        updateCheckTime.setPayment("n");
//        checktimeDao.update(updateCheckTime);
        
        // Test delete
//        checktimeDao.delete(checktimeDao.get(4));
    
    }
}
