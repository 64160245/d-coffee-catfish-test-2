/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui;

import com.Dcoffee.catfish.dao.OrderDao;
import com.Dcoffee.catfish.model.Order;
import com.Dcoffee.catfish.model.OrderProduct;
import com.Dcoffee.catfish.service.OrderProductService;
import com.Dcoffee.catfish.service.OrderService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Dimension;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author Miso
 */
public class OrderPanel extends javax.swing.JPanel {

    private OrderService orderService;
    private OrderProductService orderPrdService;
    private List<OrderProduct> ordPrdList;
    private List<Order> orderList;
    private OrderDao ordDao;
    private AbstractTableModel ordPrdTblModel;
    private final UserService userService;
    private final UtilDateModel model;

    /**
     * Creates new form OrderPanel
     */
    public OrderPanel() {
        initComponents();
        btnSearch.setIcon(new ImageIcon("img/search.png"));
        model = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.mouth", "Mouth");
        p.put("text.year", "Year");
        JDatePanelImpl dataPanel = new JDatePanelImpl(model, p);
        JDatePickerImpl dataPicker = new JDatePickerImpl(dataPanel, new DateLabelFormatter());
//        pnlDate.setPreferredSize(new Dimension(300, 200));
        pnlDate.add(dataPicker);
        model.setSelected(true);

        ordDao = new OrderDao();
        orderService = new OrderService();
        userService = new UserService();
        orderPrdService = new OrderProductService();
        orderList = orderService.getOrders();
        tblOrder.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblOrderPrd.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        UIManager.put("OptionPane.messageFont", new Font("Tahoma", Font.BOLD, 14));
        initTable();

    }

    private void initTable() {
        //tableOrder
        tblOrder.setModel(new AbstractTableModel() {
            String[] columnNames = {"ID", "TYPE MEMBER", "TOTAL", "DISCOUNT", "RECEIVE", "CHANGE", "ID USER",
                "TIMESTAMP"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return orderList.size();
            }

            @Override
            public int getColumnCount() {
                return 8;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Order order = orderList.get(rowIndex);
                //System.out.println(ordPrd);

                switch (columnIndex) {
                    case 0:
                        return order.getOrderId();
                    case 1:
                        if (order.getCusId() < 0) {
                            return "No Member";
                        } else {
                            return "Members";
                        }

                    case 2:
                        return order.getOrderTotal();
                    case 3:
                        if (order.getOrderDiscount() < 1) {
                            return 0.0;
                        } else {
                            return order.getOrderDiscount();
                        }

                    case 4:
                        return order.getOrderCash();
                    case 5:
                        if (order.getOrderChange() < 1) {
                            return 0.0;
                        } else {
                            return order.getOrderChange();
                        }
                    case 6:
//                        return userService.getName(order.getUserId());
                        return order.getUserId();
                    case 7:
                        return order.getOrderDateTime();

                    default:
                        return "Unknow";
                }

            }
        });
        
        ordPrdTblModel = new AbstractTableModel() {

            String[] columnNames = {"Product_Name", "Product_Size", "Product_Type", "Product_Level", "Amount", "Product_Price", "Total"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return ordPrdList.size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                OrderProduct ordPrd = ordPrdList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return ordPrd.getProductName();
                    case 1:
                        return ordPrd.getProductSize();
                    case 2:
                        return ordPrd.getProductType();
                    case 3:
                        return ordPrd.getProductSweetness();
                    case 4:
                        return ordPrd.getProductAmount();
                    case 5:
                        return ordPrd.getProductPrice();
                    case 6:
                        return ordPrd.getTotal();
                    default:
                        return "Unknown";
                }
            }
        };
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOrder = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblOrderPrd = new javax.swing.JTable();
        btnSearch = new javax.swing.JButton();
        pnlDate = new javax.swing.JPanel();

        setBackground(new java.awt.Color(65, 55, 54));

        jLabel1.setFont(new java.awt.Font("AngsanaUPC", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Order");

        tblOrder.setBackground(new java.awt.Color(211, 197, 186));
        tblOrder.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tblOrder.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblOrder.setToolTipText("");
        tblOrder.setRowHeight(40);
        tblOrder.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOrderMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblOrder);

        jLabel3.setFont(new java.awt.Font("AngsanaUPC", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Order_detail");

        tblOrderPrd.setBackground(new java.awt.Color(211, 197, 186));
        tblOrderPrd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblOrderPrd.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblOrderPrd.setToolTipText("");
        tblOrderPrd.setRowHeight(40);
        tblOrderPrd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblOrderPrdMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblOrderPrd);

        btnSearch.setBackground(new java.awt.Color(84, 70, 63));
        btnSearch.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("Search");
        btnSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        pnlDate.setBackground(new java.awt.Color(65, 55, 54));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1230, Short.MAX_VALUE)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlDate, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnSearch)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDate, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch))
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println(""+date.format(model.getValue()));
        String datetime = date.format(model.getValue());
        orderList = orderService.getDate(datetime);
        ((AbstractTableModel) tblOrder.getModel()).fireTableDataChanged();


    }//GEN-LAST:event_btnSearchActionPerformed

    private void tblOrderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOrderMouseClicked
        int selectedIndex = tblOrder.getSelectedRow();

        if (selectedIndex >= 0) {
            Order ord = orderList.get(selectedIndex);

            ordPrdList = orderPrdService.getOrderProductByOrdID(ord.getOrderId());
            tblOrderPrd.setModel(ordPrdTblModel);
            refreshOrdPrdTable(ord.getOrderId());
        }
    }//GEN-LAST:event_tblOrderMouseClicked

    private void tblOrderPrdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblOrderPrdMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblOrderPrdMouseClicked

    public void refreshOrdPrdTable(int id) {
        ordPrdList = orderPrdService.getOrderProductByOrdID(id);
        tblOrderPrd.revalidate();
        tblOrderPrd.repaint();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlDate;
    private javax.swing.JTable tblOrder;
    private javax.swing.JTable tblOrderPrd;
    // End of variables declaration//GEN-END:variables
}
