/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui.POS;

import com.Dcoffee.catfish.model.Customer;
import com.Dcoffee.catfish.model.Order;
import com.Dcoffee.catfish.model.OrderProduct;
import com.Dcoffee.catfish.model.Product;
import com.Dcoffee.catfish.model.Promotion;
import com.Dcoffee.catfish.model.User;
import com.Dcoffee.catfish.service.CustomerService;
import com.Dcoffee.catfish.service.OrderProductService;
import com.Dcoffee.catfish.service.OrderService;
import com.Dcoffee.catfish.service.UserService;
import com.Dcoffee.catfish.ui.MainMenuFrame;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Miso
 */
public class POSPanel extends javax.swing.JPanel {

    /**
     * Creates new form posPanel
     */
    Customer updatedCustomer;
    Customer customer;
    Promotion updatedPromotion;
    Promotion promotion;
    private User user;
    CustomerService customerService = new CustomerService();
    OrderProductService orderProductService = new OrderProductService();
    DefaultTableModel model;
    private ProductListPanel productListPanel = new ProductListPanel();
    private MainMenuFrame main = new MainMenuFrame();
    private OrderService orderServive = new OrderService();
    private int currentOrderID = orderServive.getLast().getOrderId() + 1;
    private int currentOrderProductID = orderProductService.getLast().getOrderProductId() + 1;

    public POSPanel() {
        initComponents();
        getDateTime();
        btnMemberReg.setIcon(new ImageIcon("img/adduser.png"));
        btnSearchMember.setIcon(new ImageIcon("img/user.png"));
        btnPromo.setIcon(new ImageIcon("img/promote.png"));
        btnPay.setIcon(new ImageIcon("img/salary.png"));
        btnRemove.setIcon(new ImageIcon("img/del.png"));
        iconpom.setIcon(new ImageIcon("img/pom.png"));
        iconcard.setIcon(new ImageIcon("img/card1.png"));
        iconbath.setIcon(new ImageIcon("img/bath.png"));
        tblProductList.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 11));
        tblProductList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        productListPanel = new ProductListPanel();
        scpProduct.setViewportView(productListPanel);
        productListPanel.setPOSPanel(this);
        main.setPOSPanel(this);
        UIManager.put("OptionPane.messageFont", new Font("Tahoma", Font.BOLD, 14));
        model = (DefaultTableModel) tblProductList.getModel();
        enableFunction(false);

        txtDiscount.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                getItemCost();
                if (txtReceive.getText().isBlank() == false) {
                    getChange();
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                getItemCost();
                if (txtReceive.getText().isBlank() == false) {
                    getChange();
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                getItemCost();
                if (txtReceive.getText().isBlank() == false) {
                    getChange();
                }
            }
        });

        txtCusPointUse.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (customer != null && model.getRowCount() > 0) {
                    if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > customer.getCusPoint() + 1) {
                        setTxtCustomerPointUse(customer.getCusPoint() + 1);
                    } else if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > 20) {
                        setTxtCustomerPointUse(20);
                    }
                    txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
                    txtMemberDiscount.setText(Double.toString(getCustomerPointUse() / 2));
                    getItemCost();
                    if (txtReceive.getText().isBlank() == false) {
                        getChange();
                    }
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (customer != null && model.getRowCount() > 0) {
                    if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > customer.getCusPoint() + 1) {
                        setTxtCustomerPointUse(customer.getCusPoint() + 1);
                    } else if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > 20) {
                        setTxtCustomerPointUse(20);
                    }
                    txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
                    txtDiscount.setText(Double.toString(getCustomerPointUse() / 2));
                    getItemCost();
                    if (txtReceive.getText().isBlank() == false) {
                        getChange();
                    }
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (customer != null && model.getRowCount() > 0) {
                    if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > customer.getCusPoint() + 1) {
                        setTxtCustomerPointUse(customer.getCusPoint() + 1);
                    } else if (!txtCusPointUse.getText().isEmpty() && Integer.parseInt(txtCusPointUse.getText()) > 20) {
                        setTxtCustomerPointUse(20);
                    }
                    txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
                    txtDiscount.setText(Double.toString(getCustomerPointUse() / 2));
                    getItemCost();
                    if (txtReceive.getText().isBlank() == false) {
                        getChange();
                    }
                }
            }
        });

        txtReceive.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (model.getRowCount() > 0 && !txtReceive.getText().isBlank()) {
                    getChange();
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (model.getRowCount() > 0 && !txtReceive.getText().isBlank()) {
                    getChange();
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (model.getRowCount() > 0 && !txtReceive.getText().isBlank()) {
                    getChange();
                }
            }
        });
    }

    public void getDateTime() {
        txtDateTime.setText(TOOL_TIP_TEXT_KEY);
        Timer timer;
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date = new Date();
                DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                String time = timeFormat.format(date);

                Date date2 = new Date();
                DateFormat timeFormat2 = new SimpleDateFormat("dd-MM-yyyy");
                String time2 = timeFormat2.format(date2);
                txtDateTime.setText(time2 + " " + time);
            }
        };
        timer = new Timer(1000, actionListener);
        timer.setInitialDelay(0);
        timer.start();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String dateTimeNow() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);

    }

    public Order orderDetail() {
        Order order = new Order();
        order.setOrderId(currentOrderID);
        order.setOrderDateTime(dateTimeNow());
//setCustomer
        int cusid = -1;
        if (customer != null) {
            cusid = customer.getCusId();
        }

        order.setCusId(cusid);
//setUser

        order.setUserId(UserService.getCurrentUser().getId());

        double total = 0;
        if (txtTotal1.getText().isBlank() == false) { //checkNull
            total = Double.parseDouble(txtTotal1.getText());
        }
        order.setOrderTotal(total);

        double discount = 0;
        double memberDiscount = 0;
        if (txtDiscount.getText().isBlank() == false) {
            discount = Double.parseDouble(txtDiscount.getText());
        }
        if (txtDiscount.getText().isBlank() == false) {
            memberDiscount = Double.parseDouble(txtMemberDiscount.getText());
        }

        double totalDiscount = discount + memberDiscount;
        order.setOrderDiscount(totalDiscount);

        double receive = 0;
        if (txtReceive.getText().isBlank() == false) {
            receive = Double.parseDouble(txtReceive.getText());
        }
        order.setOrderCash(receive);

        double change = 0;
        if (txtChange.getText().isBlank() == false) {
            change = Double.parseDouble(txtChange.getText());
        }
        order.setOrderChange(change);

        return order;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        updatedCustomer = customer;
        enableCustomerPointUseTxt();
        showCustomer(customer);
    }

    public void showCustomer(Customer customer) {
        txtCusPoint.setText(Integer.toString(customer.getCusPoint()));
        lblNameCus.setText(customer.getCusName() + " " + customer.getCusSurname());

        if (model.getRowCount() > 0) {
            txtCusPointReceive.setText("1");
        } else {
            txtCusPointReceive.setText("0");
        }
        if (txtCusPointReceive.getText().equals("1")) {
            txtCusPointRemain.setText(Integer.toString(customer.getCusPoint() + 1));
        } else {
            txtCusPointRemain.setText(Integer.toString(customer.getCusPoint()));
        }
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
        updatedPromotion = promotion;
        showPromotion(promotion);
    }

    private void showPromotion(Promotion promotion) {
        lblPromo.setText(Double.toString(promotion.getPromoDiscount()));
        lblNamePromo.setText(promotion.getPromoName());
        txtDiscount.setText(Double.toString(promotion.getPromoDiscount()));
    }

    public void setOrderTable(Product product, int amount, int sweetLevel, String type) {
        lblOrder.setText("Order id :" + currentOrderID);
        int price = (int) product.getProductPrice();
        if (type.equals("ร้อน")) {
            price = price - 5;
        } else if (type.equals("ปั่น")) {
            price = price + 5;
        }

        // Flag to check if the product is already in the list
        boolean productFound = false;

        for (int i = 0; i < model.getRowCount(); i++) {
            if (model.getValueAt(i, 1).equals(product.getProductName())
                    && model.getValueAt(i, 2).equals(type)
                    && (sweetLevel == 1 && model.getValueAt(i, 3).equals("น้อย")
                        || sweetLevel == 2 && model.getValueAt(i, 3).equals("ปกติ")
                        || sweetLevel == 3 && model.getValueAt(i, 3).equals("มาก"))) {
                // Product found, increment the quantity
                int currentAmount = (int) model.getValueAt(i, 4);
                int newAmount = currentAmount + amount;
                model.setValueAt(newAmount, i, 4);
                double totalPrice = price * newAmount;
                model.setValueAt(totalPrice, i, 5);
                productFound = true;
                break;
            }
        }

        if (!productFound) {
            // Product not found, add a new entry
            String sweetLevelText = "";
            switch (sweetLevel) {
                case 1:
                    sweetLevelText = "น้อย";
                    break;
                case 2:
                    sweetLevelText = "ปกติ";
                    break;
                case 3:
                    sweetLevelText = "มาก";
                    break;
                default:
                    break;
            }

            model.addRow(new Object[]{model.getRowCount() + 1, product.getProductName(), type, sweetLevelText, amount, price * amount});
        }

        if (model.getRowCount() > 0) {
            enableFunction(true);
            if (customer != null) {
                txtCusPointReceive.setText("1");
                txtCusPointUse.setEditable(true);
                txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
            }
        } else {
            enableFunction(false);
            if (customer != null) {
                txtCusPointReceive.setText("0");
                txtCusPointRemain.setText(Integer.toString(getCustomerPointRemain()));
            }
        }

        getItemCost();
    }

    public OrderProduct getOrderProductFromTable(int row) {
        OrderProduct orderProduct = new OrderProduct();
        for (int i = 0; i < tblProductList.getColumnCount(); i++) {

            String value = tblProductList.getValueAt(row, i).toString();
            if (value.equals("เย็น")) {
                value = "C";
            } else if (value.equals("ร้อน")) {
                value = "H";
            } else if (value.equals("ปั่น")) {
                value = "F";
            } else if (value.equals("น้อย")) {
                value = "1";
            } else if (value.equals("ปกติ")) {
                value = "2";
            } else if (value.equals("มาก")) {
                value = "3";
            }
            if (i == 1) {
                orderProduct.setOrderProductId(currentOrderProductID);
                orderProduct.setOrderId(currentOrderID);
                orderProduct.setProductName(value);
            } else if (i == 2) {
                orderProduct.setProductType(value);
            } else if (i == 3) {
                orderProduct.setProductSweetness(value);
            } else if (i == 4) {
                orderProduct.setProductAmount(Integer.parseInt(value));
            } else if (i == 5) {
                orderProduct.setProductPrice(Double.parseDouble(value) / orderProduct.getProductAmount());
                orderProduct.setTotal(orderProduct.getProductPrice() * orderProduct.getProductAmount());
            }

        }

        return orderProduct;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PaymentOption = new javax.swing.ButtonGroup();
        txtDateTime = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        Search = new javax.swing.JButton();
        scpProduct = new javax.swing.JScrollPane();
        lblOrder = new javax.swing.JLabel();
        btnMemberReg = new javax.swing.JButton();
        btnSearchMember = new javax.swing.JButton();
        pnlOrder = new javax.swing.JPanel();
        scpProductList = new javax.swing.JScrollPane();
        tblProductList = new javax.swing.JTable();
        btnDecrease = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        btnIncrease = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        pnlTotal2 = new javax.swing.JPanel();
        lblTotal3 = new javax.swing.JLabel();
        txtTotal1 = new javax.swing.JTextField();
        pnlTotal1 = new javax.swing.JPanel();
        iconbath = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        txtSubtotal = new javax.swing.JTextField();
        txtDiscount = new javax.swing.JTextField();
        txtTax = new javax.swing.JTextField();
        lblNameCus = new javax.swing.JLabel();
        receive = new javax.swing.JLabel();
        lblPointUse1 = new javax.swing.JLabel();
        lblPointRemain1 = new javax.swing.JLabel();
        txtCusPoint = new javax.swing.JTextField();
        txtCusPointReceive = new javax.swing.JTextField();
        txtCusPointUse = new javax.swing.JTextField();
        txtCusPointRemain = new javax.swing.JTextField();
        lblSubtotal13 = new javax.swing.JLabel();
        lblMemberDiscount2 = new javax.swing.JLabel();
        lblDiscount2 = new javax.swing.JLabel();
        lblDiscount3 = new javax.swing.JLabel();
        lblSubtotal5 = new javax.swing.JLabel();
        lblSubtotal15 = new javax.swing.JLabel();
        lblSubtotal16 = new javax.swing.JLabel();
        lblSubtotal17 = new javax.swing.JLabel();
        lblNamePromo = new javax.swing.JLabel();
        lblSubtotal19 = new javax.swing.JLabel();
        lblPromo = new javax.swing.JTextField();
        btnPromo = new javax.swing.JButton();
        lblSubtotal20 = new javax.swing.JLabel();
        txtMemberDiscount = new javax.swing.JTextField();
        lblTotal1 = new javax.swing.JLabel();
        lblPoint2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblSubtotal14 = new javax.swing.JLabel();
        rbCash = new javax.swing.JRadioButton();
        rbPromptpay = new javax.swing.JRadioButton();
        rbCard = new javax.swing.JRadioButton();
        lblReceive = new javax.swing.JLabel();
        txtReceive = new javax.swing.JTextField();
        lblChange = new javax.swing.JLabel();
        iconpom = new javax.swing.JLabel();
        iconcard = new javax.swing.JLabel();
        txtChange = new javax.swing.JTextField();
        btnPay = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();

        setBackground(new java.awt.Color(65, 55, 54));

        txtDateTime.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtDateTime.setForeground(new java.awt.Color(255, 255, 255));
        txtDateTime.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtDateTime.setText("Datetime");

        Search.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        Search.setText("Search");

        scpProduct.setBackground(new java.awt.Color(66, 52, 33));
        scpProduct.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        scpProduct.setForeground(new java.awt.Color(211, 197, 186));
        scpProduct.setMaximumSize(new java.awt.Dimension(32767, 800));
        scpProduct.setMinimumSize(new java.awt.Dimension(16, 800));

        lblOrder.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblOrder.setForeground(new java.awt.Color(255, 255, 255));
        lblOrder.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblOrder.setText("Order id ");
        lblOrder.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblOrder.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        btnMemberReg.setBackground(new java.awt.Color(84, 70, 63));
        btnMemberReg.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnMemberReg.setForeground(new java.awt.Color(255, 255, 255));
        btnMemberReg.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnMemberReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberRegActionPerformed(evt);
            }
        });

        btnSearchMember.setBackground(new java.awt.Color(84, 70, 63));
        btnSearchMember.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnSearchMember.setForeground(new java.awt.Color(255, 255, 255));
        btnSearchMember.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSearchMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchMemberActionPerformed(evt);
            }
        });

        pnlOrder.setBackground(new java.awt.Color(211, 197, 186));
        pnlOrder.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        pnlOrder.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblProductList.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblProductList.setForeground(new java.awt.Color(84, 70, 63));
        tblProductList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ลำดับ", "สินค้า", "ประเภท", "ความหวาน", "จำนวน", "ราคา"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblProductList.setSelectionBackground(new java.awt.Color(84, 70, 63));
        tblProductList.setSelectionForeground(new java.awt.Color(255, 255, 255));
        scpProductList.setViewportView(tblProductList);

        pnlOrder.add(scpProductList, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 430, 250));

        btnDecrease.setBackground(new java.awt.Color(84, 70, 63));
        btnDecrease.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDecrease.setForeground(new java.awt.Color(255, 255, 255));
        btnDecrease.setText("-");
        btnDecrease.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDecrease.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDecreaseActionPerformed(evt);
            }
        });
        pnlOrder.add(btnDecrease, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 270, 40, 30));

        btnRemove.setBackground(new java.awt.Color(84, 70, 63));
        btnRemove.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnRemove.setForeground(new java.awt.Color(255, 255, 255));
        btnRemove.setText("Remove");
        btnRemove.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });
        pnlOrder.add(btnRemove, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 270, 100, 30));

        btnIncrease.setBackground(new java.awt.Color(84, 70, 63));
        btnIncrease.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnIncrease.setForeground(new java.awt.Color(255, 255, 255));
        btnIncrease.setText("+");
        btnIncrease.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnIncrease.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIncreaseActionPerformed(evt);
            }
        });
        pnlOrder.add(btnIncrease, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 40, 30));

        btnCancel.setBackground(new java.awt.Color(102, 0, 0));
        btnCancel.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(255, 255, 255));
        btnCancel.setText("Cancel");
        btnCancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        pnlOrder.add(btnCancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 270, 90, 30));

        pnlTotal2.setBackground(new java.awt.Color(211, 197, 186));
        pnlTotal2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTotal3.setFont(new java.awt.Font("Tahoma", 1, 26)); // NOI18N
        lblTotal3.setForeground(new java.awt.Color(84, 70, 63));
        pnlTotal2.add(lblTotal3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        txtTotal1.setBackground(new java.awt.Color(211, 197, 186));
        txtTotal1.setFont(new java.awt.Font("Tahoma", 1, 22)); // NOI18N
        txtTotal1.setForeground(new java.awt.Color(84, 70, 63));
        txtTotal1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotal1.setBorder(null);
        txtTotal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotal1ActionPerformed(evt);
            }
        });
        pnlTotal2.add(txtTotal1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 200, 30));

        pnlOrder.add(pnlTotal2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 290, 105, 10));

        pnlTotal1.setBackground(new java.awt.Color(211, 197, 186));
        pnlTotal1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        pnlTotal1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        iconbath.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        iconbath.setForeground(new java.awt.Color(84, 70, 63));
        iconbath.setPreferredSize(new java.awt.Dimension(50, 30));
        pnlTotal1.add(iconbath, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 270, 40, -1));

        txtTotal.setBackground(new java.awt.Color(211, 197, 186));
        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 22)); // NOI18N
        txtTotal.setForeground(new java.awt.Color(0, 102, 51));
        txtTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotal.setBorder(null);
        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 270, 140, 30));

        txtSubtotal.setBackground(new java.awt.Color(211, 197, 186));
        txtSubtotal.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtSubtotal.setForeground(new java.awt.Color(84, 70, 63));
        txtSubtotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSubtotal.setText("0");
        txtSubtotal.setBorder(null);
        txtSubtotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSubtotalActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtSubtotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 170, 260, -1));

        txtDiscount.setBackground(new java.awt.Color(211, 197, 186));
        txtDiscount.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtDiscount.setForeground(new java.awt.Color(84, 70, 63));
        txtDiscount.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDiscount.setText("0");
        txtDiscount.setBorder(null);
        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 210, 260, -1));

        txtTax.setBackground(new java.awt.Color(211, 197, 186));
        txtTax.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtTax.setForeground(new java.awt.Color(84, 70, 63));
        txtTax.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTax.setText("0");
        txtTax.setBorder(null);
        txtTax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTaxActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtTax, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 230, 260, -1));

        lblNameCus.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblNameCus.setForeground(new java.awt.Color(0, 0, 0));
        pnlTotal1.add(lblNameCus, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 70, -1, -1));

        receive.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        receive.setForeground(new java.awt.Color(84, 70, 63));
        receive.setText("Receive");
        pnlTotal1.add(receive, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 100, -1, -1));

        lblPointUse1.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblPointUse1.setForeground(new java.awt.Color(84, 70, 63));
        lblPointUse1.setText("Use");
        pnlTotal1.add(lblPointUse1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 100, -1, -1));

        lblPointRemain1.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblPointRemain1.setForeground(new java.awt.Color(84, 70, 63));
        lblPointRemain1.setText("Point Balance");
        pnlTotal1.add(lblPointRemain1, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 100, -1, -1));

        txtCusPoint.setBackground(new java.awt.Color(211, 197, 186));
        txtCusPoint.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtCusPoint.setForeground(new java.awt.Color(84, 70, 63));
        txtCusPoint.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusPoint.setText("0");
        txtCusPoint.setBorder(null);
        txtCusPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCusPointActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtCusPoint, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 50, -1));

        txtCusPointReceive.setBackground(new java.awt.Color(211, 197, 186));
        txtCusPointReceive.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtCusPointReceive.setForeground(new java.awt.Color(84, 70, 63));
        txtCusPointReceive.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusPointReceive.setText("0");
        txtCusPointReceive.setBorder(null);
        pnlTotal1.add(txtCusPointReceive, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 120, 40, -1));

        txtCusPointUse.setBackground(new java.awt.Color(211, 197, 186));
        txtCusPointUse.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtCusPointUse.setForeground(new java.awt.Color(84, 70, 63));
        txtCusPointUse.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusPointUse.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        txtCusPointUse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCusPointUseActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtCusPointUse, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 120, 40, -1));

        txtCusPointRemain.setBackground(new java.awt.Color(211, 197, 186));
        txtCusPointRemain.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtCusPointRemain.setForeground(new java.awt.Color(84, 70, 63));
        txtCusPointRemain.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCusPointRemain.setText("0");
        txtCusPointRemain.setBorder(null);
        txtCusPointRemain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCusPointRemainActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtCusPointRemain, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 120, 50, -1));

        lblSubtotal13.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblSubtotal13.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal13.setText("Total price :");
        pnlTotal1.add(lblSubtotal13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, -1, -1));

        lblMemberDiscount2.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblMemberDiscount2.setForeground(new java.awt.Color(84, 70, 63));
        lblMemberDiscount2.setText("Member discount:");
        pnlTotal1.add(lblMemberDiscount2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, -1, -1));

        lblDiscount2.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblDiscount2.setForeground(new java.awt.Color(84, 70, 63));
        lblDiscount2.setText("VAT :");
        pnlTotal1.add(lblDiscount2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 140, -1));

        lblDiscount3.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblDiscount3.setForeground(new java.awt.Color(84, 70, 63));
        lblDiscount3.setText("Promotional discount:");
        pnlTotal1.add(lblDiscount3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 140, -1));

        lblSubtotal5.setFont(new java.awt.Font("AngsanaUPC", 3, 28)); // NOI18N
        lblSubtotal5.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal5.setText("-----------------------------------------------------------------------");
        pnlTotal1.add(lblSubtotal5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 440, 30));

        lblSubtotal15.setFont(new java.awt.Font("AngsanaUPC", 3, 28)); // NOI18N
        lblSubtotal15.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal15.setText("_Point member_______________________________");
        pnlTotal1.add(lblSubtotal15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 420, -1));

        lblSubtotal16.setFont(new java.awt.Font("AngsanaUPC", 3, 28)); // NOI18N
        lblSubtotal16.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal16.setText("_Promotion______________________________");
        pnlTotal1.add(lblSubtotal16, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 420, -1));

        lblSubtotal17.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblSubtotal17.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal17.setText("Name :");
        pnlTotal1.add(lblSubtotal17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        lblNamePromo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblNamePromo.setForeground(new java.awt.Color(84, 70, 63));
        lblNamePromo.setText("promotion 1");
        pnlTotal1.add(lblNamePromo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 40, -1, -1));

        lblSubtotal19.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblSubtotal19.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal19.setText("Discount :");
        pnlTotal1.add(lblSubtotal19, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 40, -1, -1));

        lblPromo.setBackground(new java.awt.Color(211, 197, 186));
        lblPromo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblPromo.setForeground(new java.awt.Color(84, 70, 63));
        lblPromo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lblPromo.setText("0");
        lblPromo.setBorder(null);
        pnlTotal1.add(lblPromo, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, 40, 20));

        btnPromo.setBackground(new java.awt.Color(84, 70, 63));
        btnPromo.setFont(new java.awt.Font("AngsanaUPC", 1, 18)); // NOI18N
        btnPromo.setForeground(new java.awt.Color(255, 255, 255));
        btnPromo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromoActionPerformed(evt);
            }
        });
        pnlTotal1.add(btnPromo, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 40, 40, 30));

        lblSubtotal20.setFont(new java.awt.Font("AngsanaUPC", 3, 28)); // NOI18N
        lblSubtotal20.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal20.setText("_Receipt details_______________________________");
        pnlTotal1.add(lblSubtotal20, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 420, 30));

        txtMemberDiscount.setBackground(new java.awt.Color(211, 197, 186));
        txtMemberDiscount.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtMemberDiscount.setForeground(new java.awt.Color(84, 70, 63));
        txtMemberDiscount.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMemberDiscount.setText("0");
        txtMemberDiscount.setBorder(null);
        txtMemberDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMemberDiscountActionPerformed(evt);
            }
        });
        pnlTotal1.add(txtMemberDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 190, 260, -1));

        lblTotal1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblTotal1.setForeground(new java.awt.Color(84, 70, 63));
        lblTotal1.setText("Total :");
        pnlTotal1.add(lblTotal1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, -1, -1));

        lblPoint2.setFont(new java.awt.Font("Yu Gothic UI Semibold", 0, 14)); // NOI18N
        lblPoint2.setForeground(new java.awt.Color(84, 70, 63));
        lblPoint2.setText("Total");
        pnlTotal1.add(lblPoint2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, -1, -1));

        jPanel1.setBackground(new java.awt.Color(211, 197, 186));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblSubtotal14.setFont(new java.awt.Font("AngsanaUPC", 3, 28)); // NOI18N
        lblSubtotal14.setForeground(new java.awt.Color(84, 70, 63));
        lblSubtotal14.setText("_Payment Options_______________________________");
        jPanel1.add(lblSubtotal14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 7, -1, 30));

        PaymentOption.add(rbCash);
        rbCash.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        rbCash.setForeground(new java.awt.Color(0, 0, 0));
        rbCash.setSelected(true);
        rbCash.setText("Cash");
        rbCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbCashActionPerformed(evt);
            }
        });
        jPanel1.add(rbCash, new org.netbeans.lib.awtextra.AbsoluteConstraints(7, 43, -1, -1));

        PaymentOption.add(rbPromptpay);
        rbPromptpay.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        rbPromptpay.setForeground(new java.awt.Color(0, 0, 0));
        rbPromptpay.setText("Promptpay");
        rbPromptpay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPromptpayActionPerformed(evt);
            }
        });
        jPanel1.add(rbPromptpay, new org.netbeans.lib.awtextra.AbsoluteConstraints(7, 76, -1, -1));

        PaymentOption.add(rbCard);
        rbCard.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        rbCard.setForeground(new java.awt.Color(0, 0, 0));
        rbCard.setText("Card");
        rbCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbCardActionPerformed(evt);
            }
        });
        jPanel1.add(rbCard, new org.netbeans.lib.awtextra.AbsoluteConstraints(7, 109, -1, -1));

        lblReceive.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblReceive.setForeground(new java.awt.Color(84, 70, 63));
        lblReceive.setText("Receive");
        jPanel1.add(lblReceive, new org.netbeans.lib.awtextra.AbsoluteConstraints(86, 45, -1, -1));

        txtReceive.setBackground(new java.awt.Color(211, 197, 186));
        txtReceive.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtReceive.setForeground(new java.awt.Color(84, 70, 63));
        txtReceive.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtReceive.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(84, 70, 63)));
        txtReceive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReceiveActionPerformed(evt);
            }
        });
        jPanel1.add(txtReceive, new org.netbeans.lib.awtextra.AbsoluteConstraints(157, 44, 90, -1));

        lblChange.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblChange.setForeground(new java.awt.Color(84, 70, 63));
        lblChange.setText("Change");
        jPanel1.add(lblChange, new org.netbeans.lib.awtextra.AbsoluteConstraints(253, 45, -1, -1));
        jPanel1.add(iconpom, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 77, 40, 30));
        jPanel1.add(iconcard, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 110, 100, 20));

        txtChange.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtChange.setForeground(new java.awt.Color(84, 70, 63));
        txtChange.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtChange.setBorder(null);
        txtChange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtChangeActionPerformed(evt);
            }
        });
        jPanel1.add(txtChange, new org.netbeans.lib.awtextra.AbsoluteConstraints(324, 45, 90, -1));

        btnPay.setBackground(new java.awt.Color(84, 70, 63));
        btnPay.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPay.setForeground(new java.awt.Color(255, 255, 255));
        btnPay.setText("Payment");
        btnPay.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });
        jPanel1.add(btnPay, new org.netbeans.lib.awtextra.AbsoluteConstraints(253, 96, 161, 40));

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane2.setBorder(null);
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 676, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Search, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(lblOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(txtDateTime, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnMemberReg, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSearchMember, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scpProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 824, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(pnlTotal1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblOrder)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDateTime, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Search, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnMemberReg, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearchMember, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jScrollPane2)
                        .addGap(767, 767, 767))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(scpProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 779, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 6, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlOrder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(5, 5, 5)
                                .addComponent(pnlTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchMemberActionPerformed
        SearchCustomerForm searchCustomerForm = new SearchCustomerForm();
        searchCustomerForm.setPOSPanel(this);
        searchCustomerForm.setResizable(false);
        searchCustomerForm.setLocationRelativeTo(null);
        searchCustomerForm.setMinimumSize(null);
        searchCustomerForm.setVisible(true);
    }//GEN-LAST:event_btnSearchMemberActionPerformed

    private void btnMemberRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMemberRegActionPerformed
        CreateCustomerFrame createCustomerForm = new CreateCustomerFrame();
        createCustomerForm.setResizable(false);
        createCustomerForm.setLocationRelativeTo(null);
        createCustomerForm.setMinimumSize(null);
        createCustomerForm.setVisible(true);
    }//GEN-LAST:event_btnMemberRegActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if (isTableisEmpty() == false || customer != null) {
            int result = JOptionPane.showConfirmDialog(null, "Want to cancel the current Order? ?", "Confirm cancellation of Order",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.YES_OPTION) {
                enableFunction(false);
                if (customer != null) {
                    txtCusPointReceive.setText("0");
                }
                clearCustomerDetail();
            }
        } else {
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f, "Unable to cancel current bill. Because the item is still empty.", "Unable to cancel bill", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void txtCusPointRemainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCusPointRemainActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCusPointRemainActionPerformed

    private void txtCusPointUseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCusPointUseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCusPointUseActionPerformed

    private void txtCusPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCusPointActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCusPointActionPerformed

    private void txtTaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTaxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTaxActionPerformed

    private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDiscountActionPerformed

    private void txtSubtotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSubtotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSubtotalActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void btnIncreaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIncreaseActionPerformed
        int updateRow = tblProductList.getSelectedRow();
        if (updateRow >= 0) {
            int quantity = (int) tblProductList.getValueAt(updateRow, 4);
            int price = (int) tblProductList.getValueAt(updateRow, 5);
            tblProductList.setValueAt(quantity + 1, updateRow, 4);
            tblProductList.setValueAt((price / quantity) * (quantity + 1), updateRow, 5);
            getItemCost();
        }
    }//GEN-LAST:event_btnIncreaseActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        int removeCol = tblProductList.getSelectedRow();
        if (removeCol >= 0) {
            model.removeRow(removeCol);
        }
        if (tblProductList.getRowCount() == 0) {
            enableFunction(false);
        }
        getItemCost();
        for (int i = 0; i < tblProductList.getRowCount(); i++) {
            tblProductList.setValueAt(i + 1, i, 0);
        }
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnDecreaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDecreaseActionPerformed
        int updateRow = tblProductList.getSelectedRow();
        if (updateRow >= 0) {
            int quantity = (int) tblProductList.getValueAt(updateRow, 4);
            int price = (int) tblProductList.getValueAt(updateRow, 5);
            if (quantity == 1) {
                return;
            }
            tblProductList.setValueAt(quantity - 1, updateRow, 4);
            tblProductList.setValueAt((price / quantity) * (quantity - 1), updateRow, 5);
            getItemCost();
        }
    }//GEN-LAST:event_btnDecreaseActionPerformed

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
        if (tblProductList.getRowCount() > 0) {
            if (rbCash.isSelected()) {
                if (txtReceive.getText().isBlank()) {
                    JOptionPane.showMessageDialog(null, "Please enter the amount received.", "Unable to continue", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    getChange();
                    int result = JOptionPane.showConfirmDialog(null, "Amount received : " + Double.parseDouble(txtReceive.getText()) + "\nAmount of change : " + Double.parseDouble(txtChange.getText()) + "\nPlease confirm payment\nWhen receiving from customers", "Confirm payment",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE);
                    if (result == JOptionPane.YES_OPTION) {
                        OrderProduct temp;
                        for (int i = 0; i < tblProductList.getRowCount(); i++) {
                            temp = getOrderProductFromTable(i);
                            currentOrderProductID++;
                            orderProductService.add(temp);
                        }
                        if (customer != null) {
                            getUpdatedCustomer();
                            customerService.update(updatedCustomer);
                        }
                        Order order = orderDetail();
                        currentOrderID++;
                        orderServive.add(order);
                        clearCustomerDetail();
                        enableFunction(false);

                    }
                }
            } else if (rbCard.isSelected()) {
                int result = JOptionPane.showConfirmDialog(null, "Please confirm payment.\nWhen receiving from customers", "Confirm payment",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    OrderProduct temp;
                    for (int i = 0; i < tblProductList.getRowCount(); i++) {
                        temp = getOrderProductFromTable(i);
                        currentOrderProductID++;
                        orderProductService.add(temp);
                    }
                    if (customer != null) {
                        getUpdatedCustomer();
                        customerService.update(updatedCustomer);
                    }
                    Order order = orderDetail();
                    currentOrderID++;
                    orderServive.add(order);
                    clearCustomerDetail();
                    enableFunction(false);
                }
            } else if (rbPromptpay.isSelected()) {
                int result = JOptionPane.showConfirmDialog(null, "Please confirm payment.\nWhen receiving from customers", "Confirm payment",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    OrderProduct temp;
                    for (int i = 0; i < tblProductList.getRowCount(); i++) {
                        temp = getOrderProductFromTable(i);
                        currentOrderProductID++;
                        orderProductService.add(temp);
                    }
                    if (customer != null) {
                        getUpdatedCustomer();
                        customerService.update(updatedCustomer);
                    }
                    Order order = orderDetail();
                    currentOrderID++;
                    orderServive.add(order);
                    clearCustomerDetail();
                    enableFunction(false);
                }
            }
        } else {
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f, "Unable to pay the transaction Because the item is still empty.", "Unable to pay the transaction", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnPayActionPerformed

    private void txtChangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtChangeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtChangeActionPerformed

    private void txtReceiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReceiveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtReceiveActionPerformed

    private void rbCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbCardActionPerformed
        txtReceive.setEditable(false);
        txtReceive.setText("");
        txtChange.setText("");
    }//GEN-LAST:event_rbCardActionPerformed

    private void rbPromptpayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPromptpayActionPerformed
        txtReceive.setEditable(false);
        txtReceive.setText("");
        txtChange.setText("");
    }//GEN-LAST:event_rbPromptpayActionPerformed

    private void rbCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbCashActionPerformed
        txtReceive.setEditable(true);
    }//GEN-LAST:event_rbCashActionPerformed

    private void txtTotal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotal1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotal1ActionPerformed

    private void btnPromoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromoActionPerformed
        PromotionForm promotionForm = new PromotionForm();
        promotionForm.setPOSPanel(this);
        promotionForm.setResizable(false);
        promotionForm.setLocationRelativeTo(null);
        promotionForm.setMinimumSize(null);
        promotionForm.setVisible(true);
    }//GEN-LAST:event_btnPromoActionPerformed

    private void txtMemberDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMemberDiscountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMemberDiscountActionPerformed

    public void enableFunction(boolean status) {
        btnIncrease.setEnabled(status);
        btnDecrease.setEnabled(status);
        btnRemove.setEnabled(status);
        txtDiscount.setEditable(status);
        txtCusPointUse.setEditable(status);
        txtReceive.setEditable(status);
        btnCancel.setEnabled(status);
        btnPay.setEnabled(status);
        if (status == false) {
            lblOrder.setText("Order id :");
            model.setRowCount(0);
            txtSubtotal.setText("0");
            txtDiscount.setText("0");
            txtDiscount.setText("0");
            txtChange.setText("0");
            txtCusPointRemain.setText("0");
            txtTotal.setText("0");
            txtReceive.setText("0");
            txtTax.setText("0");
            txtTotal.setText("0");
            txtCusPoint.setText("0");
            txtCusPointReceive.setText("0");
            txtCusPointUse.setText("0");
            txtReceive.setText("0");
            txtChange.setText("0");
            txtTotal1.setText("0");
        }
    }

    public Customer getUpdatedCustomer() {
        updatedCustomer.setCusId(customer.getCusId());
        updatedCustomer.setCusName(customer.getCusName());
        updatedCustomer.setCusSurname(customer.getCusSurname());
        updatedCustomer.setCusTel(customer.getCusTel());
        updatedCustomer.setCusPoint(Integer.parseInt(txtCusPointRemain.getText()));
        return updatedCustomer;
    }

    public void setTxtCustomerPointUse(int value) {
        Runnable doSet = new Runnable() {
            @Override
            public void run() {
                txtCusPointUse.setText(Integer.toString(value));
            }
        };
        SwingUtilities.invokeLater(doSet);
    }

    public void getItemCost() {
        double sum = 0;
        for (int i = 0; i < tblProductList.getRowCount(); i++) {
            sum = sum + Double.parseDouble(tblProductList.getValueAt(i, 5).toString());
        }
        txtSubtotal.setText(Double.toString(sum));
        double cTotal1 = Double.parseDouble(txtSubtotal.getText());
        double cTax = (cTotal1 * 7) / 100;
        String iTaxTotal = String.format("%.2f", cTax);
        if (!iTaxTotal.equals("0.00")) {
            txtTax.setText(iTaxTotal);
        } else {
            txtTax.setText("");
        }

        String iSubtotal = String.format("%.2f", cTotal1);
        if (!iSubtotal.equals("0.00")) {
            txtSubtotal.setText(iSubtotal);
        } else {
            txtSubtotal.setText("");
        }
        double cusDiscount = getCustomerPointUse() / 2;
        String iTotal = String.format("%.2f", cTotal1 + cTax - getDiscountValue() - cusDiscount);
        if (!iTotal.equals("0.00")) {
            txtTotal.setText(iTotal);
            txtTotal1.setText(iTotal);
        } else {
            txtTotal.setText("");
            txtTotal1.setText("");
        }
    }

    public double getDiscountValue() {
        if (txtDiscount.getText().isEmpty()) {
            return 0;
        } else {
            return Double.parseDouble(txtDiscount.getText());
        }
    }

    public int getCustomerPointRemain() {
        if (customer != null) {
            int cusPointReceive;
            if (txtCusPointReceive.getText().isEmpty()) {
                cusPointReceive = 0;
            } else {
                cusPointReceive = Integer.parseInt(txtCusPointReceive.getText());
            }
            int cusPointUse;
            if (txtCusPointUse.getText().isEmpty()) {
                cusPointUse = 0;
            } else {
                cusPointUse = Integer.parseInt(txtCusPointUse.getText());
            }
            return customer.getCusPoint() + cusPointReceive - cusPointUse;
        } else {
            return -1;
        }
    }

    public double getCustomerPointUse() {
        if (txtCusPointUse.getText().isEmpty()) {
            return 0;
        } else {
            return Double.parseDouble(txtCusPointUse.getText());
        }
    }

    public double getMemberDiscountValue() {
        if (txtDiscount.getText().isEmpty()) {
            return 0;
        } else {
            return Double.parseDouble(txtMemberDiscount.getText());
        }
    }

    public void getChange() {
        double sum = Double.parseDouble(txtTotal1.getText());
        double cash = Double.parseDouble(txtReceive.getText());

        double cChange = cash - sum;
        if (cChange < 0) {
            cChange = -1;
        }
        String changeGiven = String.format("%.2f", cChange);
        txtChange.setText(changeGiven);
    }

    public boolean isTableisEmpty() {
        if (model.getRowCount() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public void enableCustomerPointUseTxt() {
        if (model.getRowCount() > 0) {
            txtCusPointUse.setEditable(true);
        }
    }

    public void clearCustomerDetail() {
        customer = null;
        txtCusPoint.setText("");
        txtCusPointReceive.setText("");
        txtCusPointRemain.setText("");
        txtCusPointUse.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup PaymentOption;
    private javax.swing.JButton Search;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDecrease;
    private javax.swing.JButton btnIncrease;
    private javax.swing.JButton btnMemberReg;
    private javax.swing.JButton btnPay;
    private javax.swing.JButton btnPromo;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnSearchMember;
    private javax.swing.JLabel iconbath;
    private javax.swing.JLabel iconcard;
    private javax.swing.JLabel iconpom;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDiscount2;
    private javax.swing.JLabel lblDiscount3;
    private javax.swing.JLabel lblMemberDiscount2;
    private javax.swing.JLabel lblNameCus;
    private javax.swing.JLabel lblNamePromo;
    private javax.swing.JLabel lblOrder;
    private javax.swing.JLabel lblPoint2;
    private javax.swing.JLabel lblPointRemain1;
    private javax.swing.JLabel lblPointUse1;
    private javax.swing.JTextField lblPromo;
    private javax.swing.JLabel lblReceive;
    private javax.swing.JLabel lblSubtotal13;
    private javax.swing.JLabel lblSubtotal14;
    private javax.swing.JLabel lblSubtotal15;
    private javax.swing.JLabel lblSubtotal16;
    private javax.swing.JLabel lblSubtotal17;
    private javax.swing.JLabel lblSubtotal19;
    private javax.swing.JLabel lblSubtotal20;
    private javax.swing.JLabel lblSubtotal5;
    private javax.swing.JLabel lblTotal1;
    private javax.swing.JLabel lblTotal3;
    private javax.swing.JPanel pnlOrder;
    private javax.swing.JPanel pnlTotal1;
    private javax.swing.JPanel pnlTotal2;
    private javax.swing.JRadioButton rbCard;
    private javax.swing.JRadioButton rbCash;
    private javax.swing.JRadioButton rbPromptpay;
    private javax.swing.JLabel receive;
    private javax.swing.JScrollPane scpProduct;
    private javax.swing.JScrollPane scpProductList;
    private javax.swing.JTable tblProductList;
    private javax.swing.JTextField txtChange;
    private javax.swing.JTextField txtCusPoint;
    private javax.swing.JTextField txtCusPointReceive;
    private javax.swing.JTextField txtCusPointRemain;
    private javax.swing.JTextField txtCusPointUse;
    private javax.swing.JLabel txtDateTime;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtMemberDiscount;
    private javax.swing.JTextField txtReceive;
    private javax.swing.JTextField txtSubtotal;
    private javax.swing.JTextField txtTax;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtTotal1;
    // End of variables declaration//GEN-END:variables

}
