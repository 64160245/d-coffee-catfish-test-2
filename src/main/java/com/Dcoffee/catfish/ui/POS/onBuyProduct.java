/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.Dcoffee.catfish.ui.POS;

import com.Dcoffee.catfish.model.Product;


/**
 *
 * @author Miso
 */
public interface onBuyProduct {
    public void buy(Product product,int amount,int sweetLevel, String type);
}
