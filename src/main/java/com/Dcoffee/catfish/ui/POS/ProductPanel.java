/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui.POS;

import com.Dcoffee.catfish.model.Product;
import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;

/**
 *
 * @author Miso
 */
public class ProductPanel extends javax.swing.JPanel{

    private final int amount;
    ArrayList<onBuyProduct> subscriberList = new ArrayList<>();
    private final Product product;
    ButtonGroup sweetNess = new ButtonGroup();
    private Map<Product, Integer> cart = new HashMap<>();
    String type;

    /**
     * Creates new form ProductPanel
     */
    public ProductPanel(Product product) {
        initComponents();
        ImageIcon icon = new ImageIcon("./img_product/product" + product.getId() + ".png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((120.0 * width) / height), 120, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        btnCoffee.setIcon(icon);

        this.product = product;
        txtName.setText(product.getProductName());
        amount = 1;
        sweetNess.add(rdbHighSweet);
        sweetNess.add(rdbLowSweet);
        sweetNess.add(rdbNormalSweet);
        rdbNormalSweet.setSelected(true);
        type = product.getProductType();
        if (type.contains("H")) {
            int price = (int) product.getProductPrice() - 5;
            btnTypeHot.setEnabled(true);
            btnTypeHot.setText("HOT " + price);
        } else {
            btnTypeHot.setEnabled(false);
            btnTypeHot.setText(" HOT ");
        }
        if (type.contains("C")) {
            int price = (int) product.getProductPrice();
            btnTypeCool.setEnabled(true);
            btnTypeCool.setText("ICED " + price);
        } else {
            btnTypeCool.setEnabled(false);
            btnTypeCool.setText(" ICED ");
        }
        if (type.contains("F")) {
            int price = (int) product.getProductPrice() + 5;
            btnTypeFrappe.setEnabled(true);
            btnTypeFrappe.setText("FRAPPE " + price);
        } else {
            btnTypeFrappe.setEnabled(false);
            btnTypeFrappe.setText(" FRAPPE ");
        }
    }

    public void addOnBuyListener(onBuyProduct subscriber) {
        subscriberList.add(subscriber);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlProduct1 = new javax.swing.JPanel();
        pnlProduct2 = new javax.swing.JPanel();
        btnCoffee = new javax.swing.JButton();
        txtName = new javax.swing.JLabel();
        rdbHighSweet = new javax.swing.JRadioButton();
        rdbLowSweet = new javax.swing.JRadioButton();
        rdbNormalSweet = new javax.swing.JRadioButton();
        btnTypeFrappe = new javax.swing.JButton();
        btnTypeHot = new javax.swing.JButton();
        btnTypeCool = new javax.swing.JButton();

        setBackground(new java.awt.Color(211, 197, 186));
        setForeground(new java.awt.Color(211, 197, 186));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        pnlProduct1.setBackground(new java.awt.Color(211, 197, 186));
        pnlProduct1.setForeground(new java.awt.Color(211, 197, 186));

        pnlProduct2.setBackground(new java.awt.Color(211, 197, 186));
        pnlProduct2.setForeground(new java.awt.Color(211, 197, 186));
        pnlProduct2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnCoffee.setBackground(new java.awt.Color(84, 70, 63));
        btnCoffee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCoffeeActionPerformed(evt);
            }
        });
        pnlProduct2.add(btnCoffee, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 230, 181));

        txtName.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        txtName.setForeground(new java.awt.Color(0, 0, 0));
        txtName.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtName.setText("Product name");
        pnlProduct2.add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 220, 40));

        rdbHighSweet.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        rdbHighSweet.setForeground(new java.awt.Color(84, 70, 63));
        rdbHighSweet.setText("Extra sweet");
        rdbHighSweet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbHighSweetActionPerformed(evt);
            }
        });
        pnlProduct2.add(rdbHighSweet, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 280, -1, -1));

        rdbLowSweet.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        rdbLowSweet.setForeground(new java.awt.Color(84, 70, 63));
        rdbLowSweet.setText("Less sweet");
        rdbLowSweet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbLowSweetActionPerformed(evt);
            }
        });
        pnlProduct2.add(rdbLowSweet, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, -1, -1));

        rdbNormalSweet.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        rdbNormalSweet.setForeground(new java.awt.Color(84, 70, 63));
        rdbNormalSweet.setText("sweet");
        rdbNormalSweet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbNormalSweetActionPerformed(evt);
            }
        });
        pnlProduct2.add(rdbNormalSweet, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 280, -1, -1));

        btnTypeFrappe.setBackground(new java.awt.Color(84, 70, 63));
        btnTypeFrappe.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        btnTypeFrappe.setForeground(new java.awt.Color(255, 255, 255));
        btnTypeFrappe.setText("FRAPPE");
        btnTypeFrappe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTypeFrappeActionPerformed(evt);
            }
        });
        pnlProduct2.add(btnTypeFrappe, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 230, 80, 30));

        btnTypeHot.setBackground(new java.awt.Color(84, 70, 63));
        btnTypeHot.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        btnTypeHot.setForeground(new java.awt.Color(255, 255, 255));
        btnTypeHot.setText("HOT");
        btnTypeHot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTypeHotActionPerformed(evt);
            }
        });
        pnlProduct2.add(btnTypeHot, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 70, 30));

        btnTypeCool.setBackground(new java.awt.Color(84, 70, 63));
        btnTypeCool.setFont(new java.awt.Font("Tahoma", 1, 8)); // NOI18N
        btnTypeCool.setForeground(new java.awt.Color(255, 255, 255));
        btnTypeCool.setText("ICED");
        btnTypeCool.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTypeCoolActionPerformed(evt);
            }
        });
        pnlProduct2.add(btnTypeCool, new org.netbeans.lib.awtextra.AbsoluteConstraints(78, 230, 80, 30));

        javax.swing.GroupLayout pnlProduct1Layout = new javax.swing.GroupLayout(pnlProduct1);
        pnlProduct1.setLayout(pnlProduct1Layout);
        pnlProduct1Layout.setHorizontalGroup(
            pnlProduct1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlProduct2, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        pnlProduct1Layout.setVerticalGroup(
            pnlProduct1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlProduct2, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 255, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(pnlProduct1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(7, 7, 7)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 324, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(pnlProduct1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCoffeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCoffeeActionPerformed

    }//GEN-LAST:event_btnCoffeeActionPerformed

    private void rdbHighSweetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbHighSweetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbHighSweetActionPerformed

    private void rdbLowSweetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbLowSweetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbLowSweetActionPerformed

    private void rdbNormalSweetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbNormalSweetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdbNormalSweetActionPerformed

    private void btnTypeFrappeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTypeFrappeActionPerformed
        if (rdbNormalSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 2, "ปั่น");
                System.out.println(s.toString());
            }
        } else if (rdbLowSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 1, "ปั่น");
            }
        } else if (rdbHighSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 3, "ปั่น");
            }
        }
    }//GEN-LAST:event_btnTypeFrappeActionPerformed

    private void btnTypeHotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTypeHotActionPerformed
        if (rdbNormalSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 2, "ร้อน");
            }
        } else if (rdbLowSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 1, "ร้อน");
            }
        } else if (rdbHighSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 3, "ร้อน");
            }
        }
    }//GEN-LAST:event_btnTypeHotActionPerformed

    private void btnTypeCoolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTypeCoolActionPerformed
        if (rdbNormalSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 2, "เย็น");
            }
        } else if (rdbLowSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 1, "เย็น");
            }
        } else if (rdbHighSweet.isSelected()) {
            for (onBuyProduct s : subscriberList) {
                s.buy(product, amount, 3, "เย็น");
            }
        }
    }//GEN-LAST:event_btnTypeCoolActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCoffee;
    private javax.swing.JButton btnTypeCool;
    private javax.swing.JButton btnTypeFrappe;
    private javax.swing.JButton btnTypeHot;
    private javax.swing.JPanel pnlProduct1;
    private javax.swing.JPanel pnlProduct2;
    private javax.swing.JRadioButton rdbHighSweet;
    private javax.swing.JRadioButton rdbLowSweet;
    private javax.swing.JRadioButton rdbNormalSweet;
    private javax.swing.JLabel txtName;
    // End of variables declaration//GEN-END:variables

}
