/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.Dcoffee.catfish.ui;

import com.Dcoffee.catfish.model.CheckStore;
import com.Dcoffee.catfish.model.CheckStoreDetail;
import com.Dcoffee.catfish.model.Material;
import com.Dcoffee.catfish.service.CheckStoreService;
import com.Dcoffee.catfish.service.MaterialService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author kantinan
 */
public class StockCheckDialog extends javax.swing.JDialog {

    private final CheckStoreService checkStoreService;
    private CheckStore editedCheckStore;
    CheckStore checkStore;
    private List<CheckStore> checkStoreList;
    CheckStoreDetail checkStoreDetail;
    private final MaterialService mtrService;
    private List<Material> mtrList;

    /**
     * Creates new form StockCheckDialog
     */
    public StockCheckDialog(java.awt.Frame parent, CheckStore editedCheckStore) {
        super(parent, true);
        initComponents();
        checkStore = new CheckStore();
        checkStore.setUser(UserService.getCurrentUser());
        this.editedCheckStore = editedCheckStore;
        
        checkStoreService = new CheckStoreService();
        checkStoreList = checkStoreService.getCheckStores();
        mtrService = new MaterialService();
        mtrList = mtrService.getMaterials();
//        mtrList = mtrService.getMaterialsOrderByName();
        
        tblMat.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblMat.setModel(new AbstractTableModel() {
            String[] columnNames = {"ID", "NAME", "UNIT",
                "MINIMUM", "QTY", "PRICE", "TOTAL PRICE"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return mtrList.size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material mtr = mtrList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return mtr.getMaterialID();
                    case 1:
                        return mtr.getMaterialName();
                    case 2:
                        return mtr.getMaterialUnit();
                    case 3:
                        return mtr.getMaterialMin();
                    case 4:
                        return mtr.getMaterialQty();
                    case 5:
                        return mtr.getMaterialPrice();
                    case 6:
                        return mtr.getMaterialTotalPrice();

                    default:
                        return "Unknowm";
                }
            }
        });

        tblCheck.setModel(new AbstractTableModel() {
            String[] headers = {"Name",  "Qty"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return checkStore.getCheckStoreDetail().size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<CheckStoreDetail> checkStoreDetails = checkStore.getCheckStoreDetail();
                CheckStoreDetail checkStoreDetail = checkStoreDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return checkStoreDetail.getMaterialName();
                    case 1:
                        return checkStoreDetail.getMaterialQty();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<CheckStoreDetail> checkStoreDetails = checkStore.getCheckStoreDetail();
                CheckStoreDetail checkStoreDetail = checkStoreDetails.get(rowIndex);
               

            }
        });
        
        tblMat.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblMat.rowAtPoint(e.getPoint());
                int col = tblMat.columnAtPoint(e.getPoint());
                Material material = mtrList.get(row);
                
                checkStore.addCheckStoreDetail(material, 1);
                tblCheck.revalidate();
                tblCheck.repaint();
            }

        });
    }

//    private void refreshRecipt() {
//        tblCheck.revalidate();
//        tblCheck.repaint();
//        lblTotal.setText("Total: " + checkStore.get());
//    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCheck = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMat = new javax.swing.JTable();
        btnCheck = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(65, 55, 54));

        tblCheck.setBackground(new java.awt.Color(211, 197, 186));
        tblCheck.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblCheck.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblCheck);

        tblMat.setBackground(new java.awt.Color(211, 197, 186));
        tblMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblMat);

        btnCheck.setBackground(new java.awt.Color(84, 70, 63));
        btnCheck.setFont(new java.awt.Font("AngsanaUPC", 0, 24)); // NOI18N
        btnCheck.setForeground(new java.awt.Color(255, 255, 255));
        btnCheck.setText("Check");
        btnCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 592, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(76, 76, 76)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addComponent(btnCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckActionPerformed
        System.out.println("" + checkStore);
        checkStoreService.addNew(checkStore);
        
    }//GEN-LAST:event_btnCheckActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheck;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblCheck;
    private javax.swing.JTable tblMat;
    // End of variables declaration//GEN-END:variables

}
