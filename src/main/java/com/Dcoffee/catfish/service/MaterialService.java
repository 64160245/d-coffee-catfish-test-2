/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.MaterialDao;
import com.Dcoffee.catfish.model.Material;
import java.util.List;

/**
 *
 * @author HP
 */
public class MaterialService {
    
    public List<Material> getMaterials() {
        MaterialDao mrtDao = new MaterialDao();
        return mrtDao.getAll(" material_id asc");
    }
    public List<Material> getMaterialsOrderQTY() {
        MaterialDao mrtDao = new MaterialDao();
        return mrtDao.getAll(" material_qty asc");
    }
    
    public List<Material> getMaterialsOrderByName() {
        MaterialDao mrtDao = new MaterialDao();
        return mrtDao.getAll(" material_name asc");
    }

    public Material add(Material editedEmp) {
        MaterialDao mrtDao = new MaterialDao();
        return mrtDao.save(editedEmp);
    }

    public Material update(Material editedEmp) {
        MaterialDao mrtDao = new MaterialDao();
        return mrtDao.update(editedEmp);
    }

    public int delete(Material editedEmp) {
        MaterialDao mrtDao = new MaterialDao();
        return mrtDao.delete(editedEmp);
    }

    public Material getMaterialID(int id) {
        MaterialDao mrtDao = new MaterialDao();
        return mrtDao.get(id);
    }
}
