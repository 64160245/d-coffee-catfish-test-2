/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.CustomerDao;
import com.Dcoffee.catfish.dao.EmployeeDao;
import com.Dcoffee.catfish.dao.UserDao;
import com.Dcoffee.catfish.model.Customer;
import com.Dcoffee.catfish.model.Employee;
import com.Dcoffee.catfish.model.User;
import java.util.List;

/**
 *
 * @author HP
 */
public class EmployeeService {

//    public static User logintime(String login, String password) {
//        UserDao userDao = new UserDao();
//        User user = userDao.getByLogin(login);
//        System.out.println(user);
//        if (user != null && user.getPassword().equals(password)) {
//            return user;
//        }
//        return null;
//    }
    
    public List<Employee> getEmployees() {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.getAll(" employee_id asc");
    }

    public Employee add(Employee editedEmp) throws ValidateException {
        if (!editedEmp.isValid()) {
            throw new ValidateException("Employee is invaild!!!");
        }
        EmployeeDao empDao = new EmployeeDao();
        return empDao.save(editedEmp);
    }

    public Employee update(Employee editedEmp) throws ValidateException {
        if (!editedEmp.isValid()) {
            throw new ValidateException("Employee is invaild!!!");
        }
        EmployeeDao empDao = new EmployeeDao();
        return empDao.update(editedEmp);
    }

    public int delete(Employee editedEmp) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(editedEmp);
    }

    public Employee getEmployeeID(int id) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.get(id);
    }

    public  String getName(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.get(id);

        return employee.getName();
    }
    public String[] getAllName() {

        EmployeeDao employeeDao = new EmployeeDao();
        List<Employee> employee = employeeDao.getAll();

        String[] name = new String[employee.size()];

        for (int i = 0; i < employee.size(); i++) {
            name[i] = employee.get(i).getName();
        }

        return name;
    }
}
