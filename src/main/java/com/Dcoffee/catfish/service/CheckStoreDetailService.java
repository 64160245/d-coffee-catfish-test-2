/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.CheckStoreDetailDao;
import com.Dcoffee.catfish.model.CheckStore;
import com.Dcoffee.catfish.model.CheckStoreDetail;
import java.util.List;

/**
 *
 * @author HP
 */
public class CheckStoreDetailService {
    
    public List<CheckStoreDetail> getCheckStoreDetails() {
        CheckStoreDetailDao checkstoreDao = new CheckStoreDetailDao();
        return checkstoreDao.getAll(" check_detail_id asc");
    }

    public CheckStoreDetail add(CheckStoreDetail editedPromo) {
        CheckStoreDetailDao promoDao = new CheckStoreDetailDao();
        return promoDao.save(editedPromo);
    }

    public CheckStoreDetail update(CheckStoreDetail editedPromo) {
        CheckStoreDetailDao promoDao = new CheckStoreDetailDao();
        return promoDao.update(editedPromo);
    }

    public int delete(CheckStoreDetail editedPromo) {
        CheckStoreDetailDao promoDao = new CheckStoreDetailDao();
        return promoDao.delete(editedPromo);
    }

    public List<CheckStoreDetail> getCheckStoreDetailID(int id) {
        CheckStoreDetailDao promoDao = new CheckStoreDetailDao();
        return promoDao.getAll(id);
    }
    
     public List<CheckStoreDetail> getCheckStoreDetail() {
        CheckStoreDetailDao orderProductDao = new CheckStoreDetailDao();
        return orderProductDao.getAll(" order_product_id asc");
    }
     
     public List<CheckStoreDetail> getCheckStoreDetailByDetailID() {
        CheckStoreDetailDao orderProductDao = new CheckStoreDetailDao();
        return orderProductDao.getAll(" order_id asc");
    }
}
