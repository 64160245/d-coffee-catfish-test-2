/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.CheckStoreDao;
import com.Dcoffee.catfish.dao.CheckStoreDetailDao;
import com.Dcoffee.catfish.model.CheckStore;
import com.Dcoffee.catfish.model.CheckStoreDetail;
import java.util.List;

/**
 *
 * @author HP
 */
public class CheckStoreService {
    
    public List<CheckStore> getCheckStores() {
        CheckStoreDao checkstoreDao = new CheckStoreDao();
        return checkstoreDao.getAll(" check_store_id asc");
    }
        public List<CheckStore> getOrderByCheckStores() {
        CheckStoreDao checkstoreDao = new CheckStoreDao();
        return checkstoreDao.getAll(" check_store_date desc");
    }

    public CheckStore add(CheckStore editedPromo) {
        CheckStoreDao promoDao = new CheckStoreDao();
        return promoDao.save(editedPromo);
    }

    public CheckStore update(CheckStore editedPromo) {
        CheckStoreDao promoDao = new CheckStoreDao();
        return promoDao.update(editedPromo);
    }

    public int delete(CheckStore editedPromo) {
        CheckStoreDao promoDao = new CheckStoreDao();
        return promoDao.delete(editedPromo);
    }

    public CheckStore getCheckStoreID(int id) {
        CheckStoreDao promoDao = new CheckStoreDao();
        return promoDao.get(id);
    }
    
    public CheckStore addNew(CheckStore editedcheckStore) {
        CheckStoreDao checkStoreDao = new CheckStoreDao();
        CheckStoreDetailDao checkStoreDetailDao = new CheckStoreDetailDao();
        CheckStore checkStore = checkStoreDao.save(editedcheckStore);
        for(CheckStoreDetail rd: editedcheckStore.getCheckStoreDetail()) {
            rd.setCheckstoreId(checkStore.getId());
            checkStoreDetailDao.save(rd);
        }
        return checkStore;
    }
}
