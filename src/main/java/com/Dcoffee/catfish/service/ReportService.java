/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.ReportProductDao;
import com.Dcoffee.catfish.dao.ReportSaleDao;
import com.Dcoffee.catfish.dao.ReportStockDao;
import com.Dcoffee.catfish.dao.ReportTotalCustomerDao;
import com.Dcoffee.catfish.model.ReportProduct;
import com.Dcoffee.catfish.model.ReportSale;
import com.Dcoffee.catfish.model.ReportSaleD;
import com.Dcoffee.catfish.model.ReportSaleH;
import com.Dcoffee.catfish.model.ReportSaleM;
import com.Dcoffee.catfish.model.ReportSaleW;
import com.Dcoffee.catfish.model.ReportStock;
import com.Dcoffee.catfish.model.ReportTotalCustomer;
import java.util.List;

/**
 *
 * @author Miso
 */
public class ReportService {
    public List<ReportTotalCustomer> getTotalByCus() {
        ReportTotalCustomerDao reportDao = new ReportTotalCustomerDao();
        return reportDao.getTotalByCus();
    }
    public List<ReportProduct> getBestProduct() {
        ReportProductDao reportDao = new ReportProductDao();
        return reportDao.getBestProduct();
    }
    public List<ReportStock> getOutStock() {
        ReportStockDao reportDao = new ReportStockDao();
        return reportDao.getOutStock();
    }
    public List<ReportSaleM> getReportSaleByMounth() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByMounth();
    }
    public List<ReportSaleM> getReportSaleByMounth(String begin, String end) {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByMounth(begin,end);
    }
    public List<ReportSaleD> getReportSaleByDay() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByDay();
    }
     public List<ReportSaleW> getReportSaleByWeek() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByWeek();
    }
      public List<ReportSaleH> getReportSaleByHour() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByHour();
    }

    public List<ReportSale> getReportYear() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getYearReport();
    }

    public List<ReportSale> getReportMonths() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }

    public List<ReportSale> getReportSaleBySelectedYear(String year) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getSelectedYearReport(year);
    }

    public List<ReportSale> getReportPayrollMonths() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }
    
    public List<ReportSale> getReporPayrollBySelectedName(String name) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getSelectedYearReport(name);
    }

    
}
