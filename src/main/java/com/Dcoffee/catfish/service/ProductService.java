/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.ProductDao;
import com.Dcoffee.catfish.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class ProductService {
//    private ProductDao productDao = new ProductDao();
//    public ArrayList<Product> getProductsOrderByName() {
//        return (ArrayList<Product>) productDao.getAll("product_name ASC ");
//    }
    
    public Product getById(int id) {
        ProductDao productDao = new ProductDao();
        return productDao.get(id);
    }
            
    public List<Product> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }
    
    public List<Product> getProductsByName(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_name asc");
    }

    public Product addNew(Product editedProduct) throws ValidateException {
        if (!editedProduct.isValid()) {
            throw new ValidateException("Product is invaild!!!");
        }
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) throws ValidateException {
        if (!editedProduct.isValid()) {
            throw new ValidateException("Product is invaild!!!");
        }
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
}
