/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.UtilityExpenseDao;
import com.Dcoffee.catfish.model.UtilityExpense;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author HP
 */
public class UtilityExpenseService {
    public static String formatedDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
        String formatedDate = df.format(date);
        return formatedDate;
    }
    
    public List<UtilityExpense> getUtilityExpenses() {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.getAll(" utility_expense_id asc");
    }

    public UtilityExpense add(UtilityExpense editedPromo) {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.save(editedPromo);
    }

    public UtilityExpense update(UtilityExpense editedPromo) {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.update(editedPromo);
    }

    public int delete(UtilityExpense editedPromo) {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.delete(editedPromo);
    }

    public UtilityExpense getUtilityExpenseID(int id) {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.get(id);
    }
}
