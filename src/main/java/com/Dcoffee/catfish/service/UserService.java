/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.UserDao;
import com.Dcoffee.catfish.model.Employee;
import com.Dcoffee.catfish.model.User;
import java.util.List;

/**
 *
 * @author werapan
 */
public class UserService {

    //set private static
    //login get obj
    static User currentUser;

    public static User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            currentUser = user;
            return user;
        }
        return null;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    //login get obj
    public static String getUser(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.get(name);
        return user.getUsername();
    }

    public static User logintime(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
//        System.out.println(user);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_login asc");
    }
        public List<User> getOrderByUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_id asc");
    }

    public User addNew(User edtedUser) throws ValidateException {
        if (!edtedUser.isValid()) {
            throw new ValidateException("User is invaild!!!");
        }
        UserDao userDao = new UserDao();
        return userDao.save(edtedUser);
    }

    public User update(User edtedUser) throws ValidateException {
        if (!edtedUser.isValid()) {
            throw new ValidateException("User is invaild!!!");
        }
        UserDao userDao = new UserDao();
        return userDao.update(edtedUser);
    }

    public int delete(User edtedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(edtedUser);
    }
//public User getUserByLoginDetail(String username, String password, String role) {
//        UserDao userDao = new UserDao();
//        return userDao.getByLoginDetail(username, password, role);
//    }

    public String getName(int id) {
        UserDao userDao = new UserDao();
        User user = userDao.get(id);

        return user.getUsername();
    }
}
