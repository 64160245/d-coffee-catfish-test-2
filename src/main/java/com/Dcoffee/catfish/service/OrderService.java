/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.OrderDao;
import com.Dcoffee.catfish.model.Order;
import java.util.List;

/**
 *
 * @author Miso
 */
public class OrderService {



    public List<Order> getOrders() {
        OrderDao orderDao = new OrderDao();
        return orderDao.getAll(" order_id asc");
    }

    public List<Order> getOrdersByID(int id) {
        OrderDao orderDao = new OrderDao();
        return orderDao.getAll(id);
    }

    public Order getLast() {
        OrderDao orderDao = new OrderDao();
        return orderDao.getLast();
    }

    public Order add(Order editedOrder) {
        OrderDao orderDao = new OrderDao();
        System.out.println(editedOrder);
        return orderDao.save(editedOrder);
        
    }

    public int delete(Order editedOrder) {
        OrderDao orderDao = new OrderDao();
        return orderDao.delete(editedOrder);
    }
    public List<Order> getDate(String DateTime) {
        OrderDao dao = new OrderDao();
        return dao.getDate(DateTime);
    }
}
