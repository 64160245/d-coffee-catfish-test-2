/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.OrderProductDao;
import com.Dcoffee.catfish.model.OrderProduct;
import java.util.List;

/**
 *
 * @author Miso
 */
public class OrderProductService {
     public List<OrderProduct> getOrderProducts() {
        OrderProductDao orderProductDao = new OrderProductDao();
        return orderProductDao.getAll(" order_product_id asc");
    }
     
     public List<OrderProduct> getOrderProductsByOrdID() {
        OrderProductDao orderProductDao = new OrderProductDao();
        return orderProductDao.getAll(" order_id asc");
    }
     
     public List<OrderProduct> getOrderProductByOrdID(int id) {
        OrderProductDao orderProductDao = new OrderProductDao();
        return orderProductDao.getAll(id);
    }
     

    public OrderProduct getLast() {
        OrderProductDao orderProductDao = new OrderProductDao();
        return orderProductDao.getLast();
    }

    public OrderProduct add(OrderProduct editedOrderProduct) {
        OrderProductDao orderProductDao = new OrderProductDao();
        return orderProductDao.save(editedOrderProduct);
    }   
    
    public int delete(OrderProduct editedOrderProduct){
        OrderProductDao orderProductDao = new OrderProductDao();
        return orderProductDao.delete(editedOrderProduct);
    }
    public int deleteByOrderID(int ID){
        OrderProductDao orderProductDao = new OrderProductDao();
        return orderProductDao.deleteByOrderID(ID);
    }
}
