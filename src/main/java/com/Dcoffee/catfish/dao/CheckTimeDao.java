/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.CheckTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class CheckTimeDao implements Dao<CheckTime> {

    @Override
    public CheckTime get(int id) {
        CheckTime check_time = null;
        String sql = "SELECT * FROM check_time WHERE check_time_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                check_time = check_time.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return check_time;
    }

    @Override
    public List<CheckTime> getAll() {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM check_time ORDER BY check_time_id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime check_time = CheckTime.fromRS(rs);
                list.add(check_time);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckTime save(CheckTime obj) {
        String sql = "INSERT INTO check_time (check_time_in,check_time_out,check_time_hour,check_time_payment,employee_id,payroll_Id)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, df.format(obj.getIn()));
            stmt.setString(2, df.format(obj.getOut()));
            stmt.setInt(3, obj.getHour());
            stmt.setString(4, obj.getPayment());
            stmt.setInt(5, obj.getEmployeeId());
            stmt.setInt(6, obj.getPayrollId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckTime update(CheckTime obj) {
        String sql = "UPDATE check_time"
                + " SET check_time_in = ?, check_time_out = ?, check_time_hour = ?, check_time_payment = ?, employee_id = ?, payroll_id = ?"
                + " WHERE check_time_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, df.format(obj.getIn()));
            stmt.setString(2, df.format(obj.getOut()));
            stmt.setInt(3, obj.getHour());
            stmt.setString(4, obj.getPayment());
            stmt.setInt(5, obj.getEmployeeId());
            stmt.setInt(6, obj.getPayrollId());
            stmt.setInt(7, obj.getId());

            stmt.executeUpdate();
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckTime obj) {
        String sql = "DELETE FROM check_time WHERE check_time_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckTime> getAll(String where, String order) {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM check_time where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime check_time = CheckTime.fromRS(rs);
                list.add(check_time);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public static List<CheckTime> getYearReport(String year, int index) {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT *\n"
                + "FROM check_time\n"
                + "WHERE strftime(\"%Y\",check_time_in)=\"" + year + "\" and employee_id = " + index + "\n"
                + "ORDER BY strftime(\"%Y\",check_time_in) ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CheckTime item = CheckTime.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public static List<CheckTime> getMonthYearReport(String year, int index, String month) {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT *\n"
                + "FROM check_time\n"
                + "WHERE strftime(\"%Y-%m\",check_time_in)=\"" + year + "-" + month + "\" and employee_id = " + index + " and check_time_payment = \"n\" "
                + "ORDER BY strftime(\"%Y-%m\",check_time_in) ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CheckTime item = CheckTime.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public static List<CheckTime> SortPayment() {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT check_time_id,\n"
                + "       check_time_in,\n"
                + "       check_time_out,\n"
                + "       check_time_hour,\n"
                + "       check_time_payment,\n"
                + "       employee_id,\n"
                + "       payroll_id\n"
                + "FROM check_time\n"
                + "WHERE check_time_payment = 'n';";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CheckTime item = CheckTime.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
