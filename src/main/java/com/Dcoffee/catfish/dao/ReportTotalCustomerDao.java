/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.ReportProduct;
import com.Dcoffee.catfish.model.ReportTotalCustomer;
import com.Dcoffee.catfish.model.ReportTotal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class ReportTotalCustomerDao implements Dao<ReportTotal> {

    @Override
    public ReportTotal get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportTotal> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportTotal save(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportTotal update(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportTotal> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ReportTotalCustomer> getTotalByCus() {
        ArrayList<ReportTotalCustomer> list = new ArrayList();
        String sql = "SELECT customer.cus_id, customer.cus_name, customer.cus_surname, \n"
                + "       ROUND(SUM(orders.total), 2) AS total_purchases, COUNT(orders.order_id) AS total_orders\n"
                + "FROM customer\n"
                + "JOIN orders ON customer.cus_id = orders.cus_id\n"
                + "GROUP BY customer.cus_id, customer.cus_name, customer.cus_surname, customer.cus_tel\n"
                + "ORDER BY total_purchases DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportTotalCustomer obj = ReportTotalCustomer.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

}
